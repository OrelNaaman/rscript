#mivhan dugma
setwd("C:/Users/Orel Naaman/Desktop/R script")

#Question 1

churn.raw <- read.csv('churn.csv')
str(churn.raw)
summary(churn.raw)

churn <- churn.raw

dev.off()

#id attrubute provides no information 
churn$customerID <- NULL

table(churn.raw$SeniorCitizen)
#Make SeniorCitizen a factor 
churn$SeniorCitizen <- as.factor(churn$SeniorCitizen)

#Question 2

#How tenior affects chrun 
install.packages('ggplot2')
library(ggplot2)
ggplot(churn, aes(tenure, fill = Churn)) + geom_bar()
ggplot(churn, aes(tenure, fill = Churn)) + geom_bar(position = 'fill')

#remove tenior 0 from the dataset 
# Customers never churn on the when thye just begin  
filter <- churn$tenure == 0

churn <- churn[!filter,]


#does gender affect churn?
ggplot(churn, aes(gender, fill = Churn)) + geom_bar()
#almost no effect on churn 


#does contract affect churn? 
ggplot(churn, aes(Contract, fill = Churn)) + geom_bar()
ggplot(churn, aes(Contract, fill = Churn)) + geom_bar(position='fill')
#A big impact Nonth-to-Month customers tend to churn much more than one year
#And two year customers 

#precision and recall for PaperlessBilling

cf <- table(churn$PaperlessBilling, churn$Churn)

precision <- cf['Yes','Yes']/(cf['Yes','Yes'] + cf['Yes','No'] )
recall <- cf['Yes','Yes']/(cf['Yes','Yes'] + cf['No','Yes'] )


#Qeesion 3

#divide into trainig set and test set 
library(caTools)

filter <- sample.split(churn$Partner , SplitRatio = 0.7)

churn.train <- subset(churn, filter ==T)
churn.test <- subset(churn, filter ==F)


dim(churn.train)
dim(churn.test)

install.packages('rpart')
library(rpart)
install.packages('rpart.plot')
library(rpart.plot)


model <- rpart(Churn~TechSupport+tenure+Contract,churn.train)
rpart.plot(model, box.palette="RdBu", shadow.col="gray", nn=TRUE)

#calculating the confusion matrix
str(churn.test)
prediction <- predict(model,churn.test)

#we need to take the 'yes' part of the prediction
#Since the target property is a factor the opuput of predict 
#function is a matrix instead of a vectr

predict.prob.yes <- prediction[,'Yes']
prediciton.dt <- predict.prob.yes > 0.5
actual <- churn.test$Churn

cf <- table(prediciton.dt,actual)
precision <-cf['TRUE','Yes']/(cf['TRUE','Yes'] + cf['TRUE','No'] )
recall <- cf['TRUE','Yes']/(cf['TRUE','Yes'] + cf['FALSE','Yes'] )

total_errors <- (cf['TRUE','No']+cf['FALSE','Yes'])/dim(churn.test)[1]

#computing base level 
number.churn <- dim(churn[churn$Churn=='Yes',])[1]
base_level <- number.churn/dim(churn)[1]

#Model improved errors from 27% in base level to 21% with model 


#question 4

model.lr <-  glm(Churn~.-TotalCharges-MonthlyCharges-tenure,family = binomial(link = 'logit'), data = churn.train)

prediction <- predict(model.lr,churn.test, type = 'response')

actual <- churn.test$Churn
prediction.lr <- prediction >0.5

cfNB <- table(prediction.lr, actual)

precision <-cfNB['TRUE','Yes']/(cfNB['TRUE','Yes'] + cfNB['TRUE','No'] )
recall <- cfNB['TRUE','Yes']/(cfNB['TRUE','Yes'] + cfNB['FALSE','Yes'] )


#ROC curve 
install.packages('pROC')
library(pROC)

rocCurveDT <- roc(churn.test$Churn, predict.prob.yes, direction = "<", levels = c("No","Yes"))
rocCurveLR <- roc(churn.test$Churn, prediction, direction = "<", levels = c("No","Yes"))


#Calculate AUC
auc(rocCurveDT)
auc(rocCurveLR)
#Naive base is a little better


plot(rocCurveDT, col="red", main='ROC chart')
par(new=TRUE)
plot(rocCurveLR, col="blue", main='ROC chart')

#if customer conservation is costly we will prefer the model with more precison
#We will call less customers and make more sales 
#Since both models have more or less the same precision we will take the model with better recall


